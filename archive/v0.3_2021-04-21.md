Terms of Service / Community Participation Guidelines — Nudie.Social          

[← Back to Nudie.Social](https://pl.nudie.social/)

Terms of Service - Nudie.Social
-------------------------------

Version 0.3, 2021-04-21

The administration team of Nudie.Social grants usage of the Nudie.Social web site to all registered users, provided that users adhere to the Terms of Service and Community Participation Guidelines set out in this document.

The computing resources, servers that run Nudie.Social are 100% private properties. Access is granted to the naturist community in good faith. There is absolutely no warranty on the quality, service level and performance of the service.

Nudie.Social is a community-run, non-commercial web site. Nudie.Social strives to serve the naturist community, not corporate interests.

For the continual enjoyment of the service, users of Nudie.Social are required to act in good faith, in friendly, civil manner towards other users, moderators, admins, as well as other members of communities federated and connected with Nudie.Social. Nudie.Social Administrators and Moderators will swiftly ban and/or restrict access of user who carries out malicious actions towards other users, computing resources, servers, or other users on the federated network.

Nudie.Social runs on the [Pleroma microblogging platform](https://pleroma.social/). Registered Nudie.Social users can interact with with people connected to other communities and social networks that support the ActivityPub protocol, as one globally connected social network. Registered users should keep in mind that posts made on Nudie.Social public timeline are visible to web site visitors, search engine crawlers, other Nudie.Social users, as well as users on other federated servers and communities. Registered users cannot assume postings made on Nudie.Social are private in any way.

Nudie.Social do not tolerate illegal, malicious, or pirated materials. Users who post, upload or link to contents that are illegal in any one of these jurisdictions: Australia, Canada, France, Germany, New Zealand, The Netherlands, United Kingdom, and the United States, will face immediate ban without warning or explanation. Ban also applies to users who upload or link to pirated materials.

As of April 2021, users are not allowed to run robot accounts (bots) on Nudie.Social without express permission from the administrators.

Community Participation Guidelines - Nudie.Social
-------------------------------------------------

Version 0.3, 2021-04-21

Nudie.Social is a community for **naturists and nudists.** Nudie.Social welcomes contributions from people who share an interest in **naturism, nudism, social nudity, Christian naturism, nude recreation, clothes-free lifestyle, eco living, sustainable living, body acceptance, and other related topics.** Having said that, general topics and discussion are of course welcome.

Participants of this community should welcome and value healthy, civil, constructive discussions, as well as the promotion of naturism, nudism, social nudity, clothes-free lifestyle as wholesome and healthy lifestyles to the general public. As such, we require all those who participate to agree and adhere to these Community Participation Guidelines in order to help us create a safe and positive community experience for all.

These guidelines aim to support a community where all people should feel safe to participate, introduce new ideas and inspire others, regardless of:

*   Background
*   Family status
*   Gender
*   Gender identity or expression
*   Marital status
*   Sex
*   Sexual orientation
*   Native language
*   Age
*   Ability
*   Race and/or ethnicity
*   National origin
*   Socioeconomic status
*   Religion
*   Geographic location
*   Any other dimension of diversity

These guidelines exist to enable diverse individuals and groups to interact and collaborate to mutual advantage. This document outlines both expected and prohibited behaviour.

General Posting Guidelines
--------------------------

As a general "rule of thumb": If something is not appropriate IRL (in real life) in a public setting, it is probably not appropriate in the Nudie.Social community. (A naturist club is a good example of such public setting)

Guidelines on posting nude / “NSFW” content
-------------------------------------------

Being an online community for naturists and nudists, full frontal nude images / videos are considered normal on Nudie.Social. Having said that, to support the stated community value of promoting naturism, nudism, social nudity, clothes-free lifestyle as wholesome and healthy lifestyles to the general public, Nudie.Social users should exercise discretion when posting nude contents.

Images, photos, videos, animations, text, writings, or any content that have one or more of the following attributes are considered unacceptable on Nudie.Social:

*   Depict provocative action or gesture
*   Depict harmful or self-harm activity
*   Depict violence or hate speech
*   Depict illegal activity
*   Highly sexualised with an intent to arouse
*   Close-up of genital e.g. “dick pic”
*   Depict sex act or pornographic
*   Produced by pornographer, or from pornographic publication
*   Subjects’ faces are cropped out or masked
*   Lack of social or artistic context e.g. nude selfie image of one person sitting in a room
*   Of commercial or marketing nature
*   Unrealistic distortion of the human body
*   Voyeuristic
*   Taken without consent
*   Pirated or in violation copyright

Users who post content with any of the above attributes will face immediately suspension or ban with no explanation.

### Content Warning (CW) Flag

For general Fediverse etiquette please refer to the [Content Warnings section](https://docs.joinmastodon.org/user/posting/#cw) of the Mastodon User Guide. Nudie.Social users are strongly encouraged to follow the above guide and flag all nude content with the CW Flag. Using the #naturism , #nudism hash tags are also encouraged.

Expected Behaviour
------------------

The following behaviours are expected of all visitors, registered users, moderators and administrators of Nudie.Social:

### Be Respectful

Value each other’s ideas, styles and viewpoints. We may not always agree, but disagreement is no excuse for poor manners. Be open to different possibilities and to being wrong. Be kind in all interactions and communications, especially when debating the merits of different options. Be aware of your impact and how intense interactions may be affecting people. Be constructive and positive. Take responsibility for your impact and your mistakes – if someone says they have been harmed through your words or actions, listen carefully, apologise sincerely, and correct the behaviour going forward.

Respect the words and actions of the admins and moderators on Nudie.Social. Follow the instructions of the admins and moderators in a timely fashion.

### Be Inclusive

Seek diverse perspectives. Diversity of views and of people on teams powers innovation, even if it is not always comfortable. Encourage all voices. Help new perspectives be heard and listen actively. If you find yourself dominating a discussion, it is especially important to step back and encourage other voices to join in. Be aware of how much time is taken up by dominant members of the group. Provide alternative ways to contribute or participate when possible.

### Understand Different Perspectives

Our goal should not be to “win” every disagreement or argument. Strive to be an example for inclusive thinking. Step back and chill, when you see a flame war is about to start.

### Appreciate and Accommodate Our Similarities and Differences

Nudie.Social users come from many cultures and backgrounds. Cultural differences can encompass everything from official religious observances to personal habits. Be respectful of people with different cultural practices, attitudes and beliefs. Work to eliminate your own biases, prejudices and discriminatory practices. Think of others’ needs from their point of view. Use preferred titles (including pronouns) and the appropriate tone of voice. Respect people’s right to privacy and confidentiality. Be open to learning from and educating others as well as educating yourself; it is unrealistic to expect the registered users of Nudie.Social to know the cultural practices of every ethnic and cultural group, but everyone needs to recognise one’s native culture is only part of positive interactions.

Behaviour That Will Not Be Tolerated
------------------------------------

The following behaviours are considered to be unacceptable under these guidelines.

### Divisive, uncivilised, extremist, hateful language or opinions

Expression of support, sympathy or tolerance of the following will not be tolerated:

*   Cruelty, including animal cruelty
*   Fascism
*   National socialism
*   Nazism
*   Racism
*   Religious intolerance
*   Sexism
*   Terrorism
*   White supremacy or any form of racial supremacy

Offenders will face immediately suspension or ban with no explanation.

### Violence and Threats of Violence

Violence and threats of violence are not acceptable. This includes incitement of violence toward any individual, including encouraging a person to commit self-harm. This also includes posting or threatening to post other people’s personally identifying information (“doxxing”) online.

### Personal Attacks

Conflicts will inevitably arise, but frustration should never turn into a personal attack. It is not OK to insult, demean or belittle others. Attacking someone for their opinions, beliefs and ideas is not acceptable.

### Derogatory Language

Hurtful or harmful language related to:

*   Background
*   Family status
*   Gender
*   Gender identity or expression
*   Marital status
*   Sex
*   Sexual orientation
*   Native language
*   Age
*   Ability
*   Race and/or ethnicity
*   National origin
*   Socioeconomic status
*   Religion
*   Geographic location
*   Other attributes

is not acceptable. This includes deliberately referring to someone by a gender that they do not identify with, and/or questioning the legitimacy of an individual’s gender identity. If you’re unsure if a word is derogatory, don’t use it. This also includes repeated subtle and/or indirect discrimination; when asked to stop, stop the behaviour in question.

### Unwelcome Sexual Attention

Unwelcome sexual attention is not acceptable. This includes sexualised comments, jokes or imagery. This also includes intimidating another person. Simulated physical contact (such as emojis like “kiss”) without affirmative consent is not acceptable. This includes sharing or distribution of sexualised images or text. **Nude is not lewd.**

### Influencing Unacceptable Behaviour

We will treat influencing or leading such activities the same way we treat the activities themselves, and thus the same consequences apply.

Consequences of Unacceptable Behaviour
--------------------------------------

Bad behaviour from any users of Nudie.Social, including those with admin or moderation authority, will not be tolerated. All users on Nudie.Social are expected to be vigilant, call out unacceptable behaviours, and report them to the admins or moderators as soon as possible.

Anyone asked to stop unacceptable behaviour is expected to comply immediately. Violation of these guidelines can result in: Written warning, account suspension of up to 30 days, or account deletion.

In addition, any users who abuse the reporting process will be considered to be in violation of these guidelines and subject to the same consequences. False reporting, especially to retaliate or exclude, will not be accepted or tolerated.

Reporting
---------

If you believe you’re experiencing unacceptable behaviour that will not be tolerated as outlined above, please alert one or more admins and/or moderators with a direct message:

*   [Nude Ninja](https://pl.nudie.social/ninja) (administrators, moderator)

If you feel you have been unfairly accused of violating these guidelines, please follow the same reporting process.

Ask questions
-------------

Everyone is encouraged to ask questions about these guidelines. Feel free to discuss any aspect of these guidelines either on the public timeline, or via direct messages to the admins and moderators.

Privacy Policy - Nudie.Social
-----------------------------

Collection of email addresses. Nudie.Social runs on the [Pleroma platform](https://pleroma.social/), which requires new user to provide a valid email address for account verification, account activation, and for password reset purposes only. Administrators and moderators of Nudie.Social treat the email addresses as private information, and will not disclose them to any third parties. The collected email addresses will not be used to distribute unsolicited spam emails or any form of marketing / commercial communications.

Server logs. Administrators of Nudie.Social keep copies and backups of server logs for security and auditing purposes only. The server logs contain IP addresses, timestamps, URLs, and other metadata of logged in users other Fediverse instances and web site visitors. Users cannot opt-out their accounts or IP addresses from the server logs. Administrators of Nudie.Social do not disclose server logs to any third parties, or use the server logs in any form of marketing / commercial activities.

Information uploaded / posted on Nudie.Social. Users should be aware that any information posted on Nudie.Social, with the sole exception of account email addresses and passwords, will by default be publicly accessible. Such information includes, but not limit to, usernames, user avatars, user profiles, status updates (toots), boosts, uploaded images / videos / files, following / followed by user lists. Due to the federated nature of ActivityPub and related protocols, other instances or servers on the federated network may hold cached copies of the information. Administrators of Nudie.Social cannot influence the retention period of these information, or have the possibility to delete these information from the federated network.

License and attribution
-----------------------

This document is distributed under a [Creative Commons Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/3.0/).

This document have been adapted with modifications from [Version 3.0 of the Mozilla Community Participation Guidelines](https://www.mozilla.org/en-US/about/governance/policies/participation/), and the [Terms of Service of https://bsd.network/, dated October 30, 2018](https://bsd.network/terms).

Modifications to these terms and guidelines
-------------------------------------------

Nudie.Social admins may amend the terms and guidelines from time to time and may also vary the procedures it sets out where appropriate in a particular case. Your agreement to comply with the guidelines will be deemed agreement to any changes to it. In case Nudie.Social amends the guidelines, the updated version can be found [here](https://pl.nudie.social/static/terms-of-service.html).