---
gitea: none
include_toc: true
---

# Terms of Service - Nudie.Social

**DRAFT** Version 0.4, 2022-11-21 **DRAFT**

The Administration Team of Nudie.Social grants usage of [the Nudie.Social web site](https://pl.nudie.social/) to all registered members, provided that members adhere to the _Terms of Service and Community Participation Guidelines_ set out in this document.

The computing resources, servers that run Nudie.Social are 100% private properties. Access is granted to registered members, and the naturist/nudist/clothes-free community in good faith. There is absolutely no warranty on the quality, service level and performance of the service.

For the continual enjoyment of the service, members and users of Nudie.Social are required to act in good faith, in friendly, civil manner towards other members, moderators, admins, as well as other members of federated communities. Nudie.Social Administrators and Moderators will swiftly ban and/or restrict access of member who carries out malicious actions towards other members, computing resources, servers, or other users on the federated network.

Nudie.Social runs on the [Akkoma microblogging platform](https://akkoma.social/). Registered Nudie.Social members can interact with people connected to other communities and social networks that support the _ActivityPub protocol,_ as one globally connected network. Registered members should keep in mind that posts made on Nudie.Social timelines are visible to anonymous web site visitors, search engine crawlers, other Nudie.Social members, as well as users on other federated servers and communities. Registered members cannot assume postings made on Nudie.Social are private in any way.

The Administration Team of Nudie.Social do not tolerate illegal, malicious, or pirated materials. Registered members who post, upload, or link to contents that are illegal in any one of these jurisdictions: Australia, Canada, France, Germany, New Zealand, The Netherlands, United Kingdom, and the United States, will face immediate ban without warning. Ban also applies to member who upload or link to pirated materials & malicious web sites.

As of November 2022, members are not allowed to run robot accounts (bots) on Nudie.Social without express permission from the Administration Team.

Nudie.Social is a community-run, non-commercial web site, and strives to serve the naturist/nudist/clothes-free community, not corporate or business interests. Content creators, small business owners are welcome to register as members of Nudie.Social, provided their contents strongly align with the Community Participation Guidelines.

# Community Participation Guidelines - Nudie.Social

**DRAFT** Version 0.4, 2022-11-21 **DRAFT**

_Nudie.Social_ is a community for **naturists, nudists, and people who take interests in clothes-free, clothing optional living.** Nudie.Social welcomes contributions from people who share an interest in **naturism, nudism, social nudity, Christian naturism, nude recreation, clothes-free lifestyle, eco living, sustainable living, body acceptance, and other related topics.** And of course, general topics and discussion are welcome, too.

Registered members should value **healthy, civil, constructive discussions,** as well as the promotion of **naturism, nudism, social nudity, clothes-free lifestyle, as wholesome and healthy to the general public.** As such, all registered members must adhere to these Community Participation Guidelines, in order to ensure a safe and positive experience for all participants.

These guidelines aim to support a community where all people should feel safe to participate, introduce new ideas and inspire others, regardless of:

*   Background
*   Family status
*   Gender
*   Gender identity or expression
*   Marital status
*   Sex
*   Sexual orientation
*   Native language
*   Age
*   Ability
*   Race and/or ethnicity
*   National origin
*   Socioeconomic status
*   Religion
*   Geographic location
*   Any other dimension of diversity

These guidelines exist to enable diverse individuals and groups to interact and collaborate to mutual advantage. This document outlines both expected and prohibited behaviour.

## General Posting Guidelines

_Rule of Thumb Number One:_ If something is not appropriate IRL (in real life) in a public setting, it is probably not appropriate to post on Nudie.Social. (A naturist club is a good example of such public setting)

_Rule of Thumb Number Two:_ When in doubt - Check with a moderator!

The Administration Team of Nudie.Social has _zero tolerance_ towards content, or link to content, with any of these attributes:

* Depict illegal activity
* Depict violence or hate speech
* Depict harmful or self-harm activity
* Any attribute listed in the _Behaviour That Will Not Be Tolerated_ section of this document

Offenders will face immediately account ban.

## Guidelines on posting content contains nudity

Nudie.Social recognises that each member have different life circumstances. Therefore, posting nude content is _welcome, but totally optional._ Members are free to share as much, or as little, as they feel comfortable with.

To support the stated community value of **promoting naturism, nudism, social nudity, clothes-free lifestyle as wholesome and healthy to the general public,** Nudie.Social registered members should exercise discretion when posting nude contents.

Content with any of the following attributes are unacceptable on Nudie.Social:

*   Depict provocative action or gesture
*   Close-up of genital e.g. “dick pic”
*   Depict sex act or pornographic
*   Highly sexualised with an intent to arouse
*   Produced by pornographer, or from pornographic publication
*   Lack of social or artistic context e.g. nude selfie image of one person sitting in a room
*   Of commercial or marketing nature
*   Unrealistic distortion of the human body
*   Voyeuristic
*   Taken without consent
*   Pirated or in violation of copyright

In respect of members who need anonymity / privacy, Nudie.Social _tolerates, but not recommend_ content with the following attributes:

* Subjects' faces cropped out or masked

Nudie.Social moderators and administrators actively moderate content, and issue warning and/or suspension to members who posted unacceptable content (or links/URLs to such content). Repeated offenders will face ban.

## Content Warning (CW) Line

Content Warning (CW) is an important Fediverse etiquette, helping people to have autonomy in what content to see or engage.

To all Nudie.Social members: When posting a **Public, Unlisted, or Followers-Only post,** you are required to put a CW line for **all sensitive content, including, but not limited to, nudity.**

Here are some examples of concise CW lines:

* `current affair`
* `eye contact`
* `mention of death`
* `naturist activities`
* `nudity`
* `tv show/movie spoilers`
* `uspol` (pol is the shorthand for _politics_)

See below screenshot with the Public (globe icon), Unlisted (unlocked padlock icon), and Followers-Only (locked padlock icon) selections highlighted:

![Followers Only, Unlised, Public posting highlighted](./img/nudie_social_posting.png)

Attention: Repeated failure to apply CW line for nudity or other sensitive content will result in account suspension or ban.

For reference, Mastodon User Guide has a good introduction of [Content Warnings](https://docs.joinmastodon.org/user/posting/#cw)

### Content Warning Exceptions

When posting nude content in a Direct Message (envelope icon) or Local-only (people icon) post, **CW is optional.**

Regardless of posting option, CW is highly recommended for other sensitive topics, e.g. politics, spoilers, etc.

Side note: Using `#naturism , #nudism , #FKK` or related hashtags is highly recommended for Public, Unlisted posts. Hashtags help other Fediverse users to discover posts easily.

## Expected Behaviour

The following behaviours are expected of all visitors, registered members, moderators and administrators of Nudie.Social:

### Be Respectful

Value each other's ideas, styles and viewpoints. We may not always agree, but disagreement is no excuse for poor manners. Be open to different possibilities and to being wrong. Be kind in all interactions and communications, especially when debating the merits of different options. Be aware of your impact and how intense interactions may be affecting people. Be constructive and positive. Take responsibility for your impact and your mistakes - if someone says they have been harmed through your words or actions, listen carefully, apologise sincerely, and correct the behaviour going forward.

Respect the words and actions of the admins and moderators on Nudie.Social. Follow the instructions of the admins and moderators in a timely fashion.

### Be Inclusive

Seek diverse perspectives. Diversity of views and of people on teams powers innovation, even if it is not always comfortable. Encourage all voices. Help new perspectives be heard and listen actively. If you find yourself dominating a discussion, it is especially important to step back and encourage other voices to join in. Be aware of how much time is taken up by dominant members of the group. Provide alternative ways to contribute or participate when possible.

### Understand Different Perspectives

Our goal should not be to “win” every disagreement or argument. Strive to be an example for inclusive thinking. Step back and chill, when you see a flame war is about to start.

### Appreciate and Accommodate Our Similarities and Differences

Nudie.Social members come from many cultures and backgrounds. Cultural differences can encompass everything from official religious observances to personal habits. Be respectful of people with different cultural practices, attitudes and beliefs. Work to eliminate your own biases, prejudices and discriminatory practices. Think of others’ needs from their point of view. Use preferred titles (including pronouns) and the appropriate tone of voice. Respect people’s right to privacy and confidentiality. Be open to learning from and educating others as well as educating yourself; it is unrealistic to expect the registered members of Nudie.Social to know the cultural practices of every ethnic and cultural group, but everyone needs to recognise one’s native culture is only part of positive interactions.

## Behaviour That Will Not Be Tolerated

The following behaviours are considered to be unacceptable under these guidelines.

### Divisive, uncivilised, extremist, hateful language or opinions

Expression of support, sympathy or tolerance of the following will not be tolerated:

*   Cruelty, including animal cruelty
*   Fascism
*   National socialism
*   Nazism
*   Racism
*   Religious intolerance
*   Sexism
*   Terrorism
*   White supremacy or any form of racial supremacy

Offenders will face immediately ban.

### Violence and Threats of Violence

Violence and threats of violence are not acceptable. This includes incitement of violence toward any individual, including encouraging a person to commit self-harm. This also includes posting or threatening to post other people’s personally identifying information (“doxxing”) online.

### Personal Attacks

Conflicts will inevitably arise, but frustration should never turn into a personal attack. It is not OK to insult, demean or belittle others. Attacking someone for their opinions, beliefs and ideas is not acceptable.

### Derogatory Language

Hurtful or harmful language related to:

*   Background
*   Family status
*   Gender
*   Gender identity or expression
*   Marital status
*   Sex
*   Sexual orientation
*   Native language
*   Age
*   Ability
*   Race and/or ethnicity
*   National origin
*   Socioeconomic status
*   Religion
*   Geographic location
*   Other attributes

is not acceptable. This includes deliberately referring to someone by a gender that they do not identify with, and/or questioning the legitimacy of an individual's gender identity. If you’re unsure if a word is derogatory, don’t use it. This also includes repeated subtle and/or indirect discrimination; when asked to stop, stop the behaviour in question.

### Unwelcome Sexual Attention

Unwelcome sexual attention and/or harrasment is not acceptable. This includes sexualised comments, jokes or imagery. This also includes intimidating another person. Simulated physical contact (such as emojis like “kiss”) without affirmative consent is not acceptable. This includes sharing or distribution of sexualised images or text. **Nude is not lewd.**

### Influencing Unacceptable Behaviour

We will treat influencing or leading such activities the same way we treat the activities themselves, and thus the same consequences apply.

### Consequences of Unacceptable Behaviour

Bad behaviour from any members of Nudie.Social, including those with admin or moderation authority, will not be tolerated. All members on Nudie.Social are expected to be vigilant, call out unacceptable behaviours, and report them to the admins or moderators as soon as possible.

Anyone asked to stop unacceptable behaviour is expected to comply immediately. Violation of these guidelines can result in: Written warning, account suspension of up to 30 days, or account deletion.

In addition, any members who abuse the reporting process will be considered to be in violation of these guidelines and subject to the same consequences. False reporting, especially to retaliate or exclude, will not be accepted or tolerated.

## Reporting

If you believe you’re experiencing unacceptable behaviour that will not be tolerated as outlined above, please alert one or more admins and/or moderators with a direct message:

*   [Ninja](https://pl.nudie.social/ninja) (administrator, moderator)

If you feel you have been unfairly accused of violating these guidelines, please follow the same reporting process.

## Ask questions

Everyone is encouraged to ask questions about these guidelines. Feel free to discuss any aspect of these guidelines either on the public timeline, or via direct messages to the admins and moderators.

# Privacy Policy - Nudie.Social

**DRAFT** Version 0.4, 2022-11-21 **DRAFT**

Collection of email addresses. Nudie.Social runs on the [Akkoma platform](https://akkoma.social/), which requires new user to provide a valid email address for account verification, account activation, and for password reset purposes only. Administrators and moderators of Nudie.Social treat the email addresses as private information, and will not disclose them to any third parties. The collected email addresses will not be used to distribute unsolicited spam emails or any form of marketing / commercial communications.

Server logs. Administration Team of Nudie.Social keep copies and backups of server logs for security and auditing purposes only. The server logs contain IP addresses, timestamps, URLs, and other metadata of logged in users other Fediverse instances and web site visitors. Users cannot opt-out their accounts or IP addresses from the server logs. Administrators of Nudie.Social do not disclose server logs to any third parties, or use the server logs in any form of marketing / commercial activities.

Information uploaded / posted on Nudie.Social. Users should be aware that any information posted on Nudie.Social, with the sole exception of account email addresses and passwords, will by default be publicly accessible. Such information includes, but not limit to, usernames, user avatars, user profiles, status updates (posts / toots), re-post (boosts), uploaded images / videos / files, following / followed by user lists. Due to the federated nature of ActivityPub and related protocols, other instances or servers on the federated network may hold cached copies of the information. Administration Team of Nudie.Social cannot influence the retention period of these information, or have the ability to delete these information from the federated network.

# License and attribution

This document is distributed under a [Creative Commons Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/3.0/).

This document have been adapted with modifications from [Version 3.0 of the Mozilla Community Participation Guidelines](https://www.mozilla.org/en-US/about/governance/policies/participation/), and the [Terms of Service of https://bsd.network/, dated October 30, 2018](https://bsd.network/terms).

# Modifications to these terms and guidelines

Nudie.Social admins may amend the terms and guidelines from time to time and may also vary the procedures it sets out where appropriate in a particular case. Your agreement to comply with the guidelines will be deemed agreement to any changes to it. In case Nudie.Social amends the guidelines, the updated version can be found [here](https://codeberg.org/Nudie.Social/tos-community-guidelines/).
